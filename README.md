### Installation

#### source install:
- build-essential, zlib1g-dev, libreadline-dev, bison, flex
- foma : https://fomafst.github.io
- graphviz

#### docker install:
    docker build -t foma .

### Use

#### docker use:

    >docker run -ti --volume /Users/mangeot/docker/na-fst:/workdir foma
    >cp /na-fst/* /workdir/.
    >cd /workdir
    
#### foma use:

	>cp na.foma na-src.foma
	>sed -i 's/^quit$//' na-src.foma
    >foma -l na-src.foma
    foma[1]: up njɤ˩ ʑi˩ bi˩ -zo˧ -ho˩
    
#### flookup use:

	>echo "njɤ˩ ʑi˩ bi˩ -zo˧ -ho˩" | flookup na.fsm