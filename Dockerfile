##############################################################################
# Dockerfile to build foma and run the nu morphosyntactic fst parser
# Based on debian
#############################################################################
#
# Build part
#

FROM debian:stable

ENV LC_ALL C.UTF-8

RUN apt-get update && apt-get install -y build-essential \
	zlib1g-dev \
	libreadline-dev \
	bison \
	flex \
	subversion \
	graphviz

RUN svn checkout --trust-server-cert https://github.com/mhulden/foma/trunk/foma

WORKDIR /foma

RUN make && make install

WORKDIR /na-fst

COPY . .

RUN foma -l na.foma

RUN dot -T png -O na.dot

WORKDIR /workdir